import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import React from 'react';
export default function CarouselMovie() {

    const responsive = {
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 1,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        }
    };



    let renderCarouselMovie = () => {

        return (
            <div className='py-10' >


                <Carousel
                    swipeable={false}
                    draggable={false}
                    showDots={true}
                    responsive={responsive}
                    ssr={true} // means to render carousel on server-side.
                    infinite={true}
                    // autoPlay={this.props.deviceType !== "mobile" ? true : false}
                    autoPlaySpeed={3000}
                    keyBoardControl={true}
                    customTransition="all .5"
                    transitionDuration={500}
                    containerClass="carousel-container"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                    // deviceType={this.props.deviceType}
                    dotListClass="custom-dot-list-style"
                >
                    <div className='bg-black' >

                        <img className='h-full w-full rounded rounded-3xl' src="https://s3img.vcdn.vn/123phim/2021/04/lat-mat-48h-16177782153424.png" alt="" />
                    </div>
                    <div>
                        <img className='h-full w-full rounded rounded-3xl' src="https://s3img.vcdn.vn/123phim/2021/04/ban-tay-diet-quy-evil-expeller-16177781815781.png" alt="" />
                    </div>
                    <div>
                        <img className='h-full w-full rounded rounded-3xl' src="https://s3img.vcdn.vn/123phim/2021/04/nguoi-nhan-ban-seobok-16177781610725.png" alt="" />
                    </div>

                </Carousel>;



            </div>
        )
    }
    return (
        <div>{renderCarouselMovie()}</div>
    )
}
