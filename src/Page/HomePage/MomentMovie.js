import moment from 'moment';
import React from 'react'
import { NavLink } from 'react-router-dom';


export default function MomentMovie({ phim }) {
    return (
        <div className="flex space-x-10 p-5 ">
            <NavLink to={`detail/${phim.maPhim}`} >
                <img className="w-24 h-42 object-cover " src={phim.hinhAnh} alt="" />
            </NavLink>
            <div>
                <p className="font-medium text-2xl text-white mb-5">
                    {phim.tenPhim}
                </p>
                <div className="grid grid-cols-4 gap-4">
                    {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu, index) => {
                        return (
                            <div key={index}
                                className="
                  bg-red-600 text-white p-1 rounded
                "
                            >
                                {moment(lichChieu.ngayChieuGioChieu).format("DD-MM")}

                                <span className="text-yellow-400 font-bold text-base ml-5">
                                    {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                                </span>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>

    )
}
