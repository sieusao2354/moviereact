import React from 'react'

export default function AppMovie() {


    let renderDetail = () => {
        return (
            <div className='py-10  ' style={{ background: "url(./image/backApp.png)" }} >
                <div className='flex container' >
                    <div className=' p-32 text-white '>
                        <p className=' px-20 font-medium text-4xl'> Ứng dụng tiện lợi dành cho <br />

                            người yêu điện ảnh</p>
                        <p className='py-5 px-20 text-lg'>Không chỉ đặt vé, bạn còn có thể bình luận phim,  <br /> chấm điểm rạp và đổi quà hấp dẫn.</p>
                        <br />
                        <div className='px-20'>
                            <button className='px-10 bg-red-500 py-4 rounded text-2xl hover:bg-red-600'>


                                <a className='hover:text-white' target="_blank" href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197">APP MIỄN PHÍ - TẢI VỀ NGAY</a> </button>

                            <p className="py-3" >TIX có 2 phiên bản
                                <a target="_blank" className='underline' href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197"> IOS </a>
                                và <a target="_blank" className='underline' href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123">Android</a> </p>
                        </div>
                    </div>
                    <div >
                        <img className='absolute px-1 ' style={{ width: 250 }} src="./imageApp/download.png" alt="" />

                        <img className=' relative rounded-3xl my-2.5 mx-2  p-1.5 ' style={{ width: 235 }} src="./imageApp/banner.png" alt="" />


                    </div>


                </div>
            </div>
        )

    }


    return (
        <div >
            {renderDetail()}

        </div >




    )
}




