import React, { Component } from 'react'
import OptionPage from '../OptionPage/OptionPage'
import AppMovie from './AppMovie'
import CarouselMovie from './CarouselMovie'
import ListMovie from './ListMovie'
import TabMovie from './TabMovie'

export default class HomePage extends Component {
    render() {
        return (
            <div className='backGroundColor-all '>
                <CarouselMovie />
                <ListMovie />
                <TabMovie />
                <AppMovie />
                <OptionPage />
            </div>
        )
    }
}

