import React, { useEffect, useState } from 'react'

import { Tabs } from 'antd';
import { movieService } from '../../Service/MovieService';
import MomentMovie from './MomentMovie';
const { TabPane } = Tabs;

export default function TabMovie() {

    const [dataMovie, setDataMovie] = useState([])


    useEffect(() => {
        movieService.getTabMovie()
            .then((res) => {

                setDataMovie(res.data.content)
            })
            .catch((err) => {
            });
    }, [])

    const onChange = (key) => {
        console.log('key: ', key);

    }

    const renderContent = () => {
        return dataMovie.map((heThongRap, index) => {
            return (
                <TabPane
                    tab={<img className="w-16" src={heThongRap.logo} />}
                    key={index}
                >
                    <Tabs
                        style={{ height: 500 }}
                        tabPosition="left"
                        defaultActiveKey="1"
                        onChange={onChange}
                    >
                        {heThongRap.lstCumRap.map((cumRap, index) => {
                            return (
                                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                                    <div
                                        style={{ height: 500, overflowY: "scroll" }}
                                        className="shadow">


                                        {cumRap.danhSachPhim.map((phim, index) => {
                                            return <MomentMovie phim={phim} key={index} />
                                        })
                                        }
                                    </div>
                                </TabPane>
                            );
                        })
                        }
                    </Tabs >
                </TabPane >
            );
        });
    };





    const renderTenCumRap = (cumRap) => {
        return (
            <div className="text-left w-60 rounded rounded-2 ">
                <p className="text-green-500 truncate">{cumRap.tenCumRap}</p>
                <p className=" text-white truncate">{cumRap.diaChi}</p>
                <button className="text-red-500">[ Xem chi tiết ] </button>
            </div>
        );
    };

    return (
        <div className=' container mx-auto py-10 rounded rounded-2 bg-tab  '>
            <Tabs style={{ height: 500 }} defaultActiveKey="1"
                tabPosition="left"
                onChange={onChange}
            >
                {renderContent()}
            </Tabs>
        </div>
    )
}
