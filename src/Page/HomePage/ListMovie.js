import React, { useEffect, useState } from 'react'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useDispatch, useSelector } from 'react-redux';
import { getMovieActionService } from '../../redux/actions/movieAction';
import "./listMovie.css"

import { Card } from 'antd';

import { NavLink } from 'react-router-dom';

export default function ListMovie() {
    const { Meta } = Card;

    const [isOpen, setOpen] = useState(true)

    let dispatch = useDispatch()

    let { movieList } = useSelector((state) => {

        return state.MovieReducers
    });

    console.log('movieList: ', movieList);
    useEffect(() => {

        dispatch(getMovieActionService())
    }, [])



    let renderlistMovie = () => {
        return movieList.map((item, index) => {
            return (
                <div key={index} >
                    <div className='bg-[#2b2c33] grid md:grid-cols-1 p-2 mx-1 rounded-md'>

                        <div className='hover:shadow-md hover:shadow-slate-100 hover:scale-110 hover:duration-500 h-full flex items-center justify-center rounded-md'>
                            <img className='rounded-md object-cover top-0  w-full  h-52  md:h-80'
                                src={item.hinhAnh}
                                alt="..."
                            />
                        </div>
                        <div className="flex pt-2 h-16">
                            <p className="space-x-4  py-1 px-2">
                                <span className="px-2 py-1 bg-yellow-400 text-black  rounded-md font-bold">
                                    {item.maNhom}</span>
                                <span className="text-slate-500">|</span>
                                <span className="text-center text-white text-sm md:text-lg font-bold">
                                    {item.tenPhim}
                                </span>
                            </p>

                        </div>
                        <NavLink to={`detail/${item.maPhim}`}>

                            <div className=" mt-2">
                                <button className="rounded w-full md:rounded-lg bg-red-700 py-1 px-2 md:px-5 md:py-2.5 text-center text-sm font-medium text-white hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                                >Xem chi tiết
                                </button>
                            </div>
                        </NavLink>

                    </div>
                </div>

            )
        })
    }


    const responsive = {
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 5,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 3,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 2,
        }
    };

    let handleOpen = () => {
        setOpen(false)
    }










    return (
        <div>
            <div className='bg-[#202020]  container mx-auto py-8 my-4 rounded rounded-lg '>
                <div className=' flex justify-between py-2  px-20 items-center '>
                    <p className='  text-white font-medium text-2xl '>Danh sách phim đang chiếu</p>
                    <div className=' mx-10 font-medium text-xl '>

                        {isOpen ?
                            (<button className='text-red-500  ' onClick={handleOpen} >
                                {"Xem tất cả "}
                            </button>)
                            :
                            (<button className='text-yellow-500' onClick={() => { setOpen(true) }} >
                                {"Thu gọn "}
                            </button>)}

                    </div >

                </div>
                <div className='mt-2 sm:mt-4 px-2 sm:px-4'>
                    {isOpen ?
                        (
                            <Carousel
                                swipeable={false}
                                draggable={false}
                                // showDots={true}
                                responsive={responsive}
                                ssr={true} // means to render carousel on server-side.
                                infinite={true}
                                autoPlay={true}
                                autoPlaySpeed={3000}

                                keyBoardControl={true}
                                customTransition="all 1.5"
                                transitionDuration={1}
                                containerClass="carousel-container"
                                removeArrowOnDeviceType={["tablet", "mobile"]}
                                dotListClass="custom-dot-list-style"
                                className="max-w-7xl mx-auto"

                            >

                                {renderlistMovie()}


                            </Carousel>

                        )
                        :
                        (<div className="max-w-7xl mx-auto" >
                            <div className='grid grid-cols-2  sm:grid-cols-3 lg:grid-cols-5 '>

                                {renderlistMovie()}

                            </div>
                        </div>)}

                </div>


            </div >
        </div >
    )

}

// 