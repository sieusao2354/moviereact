import React from 'react'
import Lottie from 'lottie-react'

export default function LogoAnimate() {
    return (
        <div className='' onClick={() => { window.location.href = "/" }}>
            <button style={{ width: 100 }}>
                <img src='./image/logoMovie.png' alt="" />
            </button>
        </div>
    )
}