
export default function OptionPage() {






    let renderOptionPage = () => {
        return (
            <div className=" optionBackground">
                <div className=' container py-5 h-full w-full mx-auto flex font-medium '>
                    <div className=' TIX px-10'>
                        <div className='flex text-white  mx-auto'>
                            <p className=''>TIX</p>
                        </div>
                        <div className='flex text-gray-400 '>

                            <div>
                                <p className='TIX'>FAQ</p>
                                <p className='TIX'>Brand Guidelines</p>
                            </div>
                            <div className='px-10'>
                                <p className='TIX'>
                                    Thỏa thuận sử dụng</p>
                                <p className='TIX'>
                                    Chính sách bảo mật</p>
                            </div>
                        </div>
                    </div>

                    <div className=" px-20">
                        <div className=' text-white  mx-auto' >
                            <p>ĐỐI TÁC </p>
                        </div>
                        <div className='py-4 ' >
                            <div className='flex w-8 space-x-10' >
                                <img className="optionLogo" src="./imageOptionMovie/download.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download1.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download2.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download3.png" alt="" />
                            </div>
                            <div className='py-3 flex w-8 space-x-10' >
                                <img className="optionLogo" src="./imageOptionMovie/download5.png" alt="" />

                                <img className="optionLogo rounded rounded-full" src="./imageOptionMovie/download6.png" alt="" />
                                <img className="optionLogo ounded rounded-full" src="./imageOptionMovie/download7.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download8.png" alt="" />
                            </div>
                            <div className=' py-3 flex w-8 space-x-10' >
                                <img className="optionLogo" src="./imageOptionMovie/download9.png" alt="" />
                                <img className=" optionLogo rounded rounded-full" src="./imageOptionMovie/download10.png" alt="" />
                                <img className=" optionLogo rounded rounded-full" src="./imageOptionMovie/download11.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download12.png" alt="" />
                            </div>
                            <div className=' py-3 flex w-8 space-x-10' >
                                <img className="optionLogo rounded rounded-full" src="./imageOptionMovie/download13.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download14.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download15.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download16.png" alt="" />
                            </div>
                            <div className=' py-3 flex w-8 space-x-10' >
                                <img className="optionLogo" src="./imageOptionMovie/download17.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download18.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download19.png" alt="" />
                                <img className="optionLogo" src="./imageOptionMovie/download4.png" alt="" />


                            </div>


                        </div>


                    </div>
                    <div className="px-52">
                        <div className="text-white  mx-auto">
                            <p>MOBILE APP</p>
                        </div>
                        <div className="flex space-x-4">
                            <a target="_blank" href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197">
                                <img className="w-8 py-2 " src="./imageOptionMovie/Ios-image.png" alt="" />

                            </a>
                            <a target="_blank" className=" space-x-4" href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123">
                                <img className="w-8 py-2 " src="./imageOptionMovie/Android-image.png" alt="" />

                            </a>
                        </div>

                    </div>
                    <div className="">
                        <div className="text-white  mx-auto">
                            <p>MOBILE APP</p>
                        </div>
                        <div className="flex space-x-4">
                            <a target="_blank" href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197">
                                <img className="w-8 py-2 " src="./imageOptionMovie/Facebook-image.png" alt="" />

                            </a>
                            <a target="_blank" className=" space-x-4" href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123">
                                <img className="w-8 py-2 " src="./imageOptionMovie/Zalo-image.png" alt="" />

                            </a>
                        </div>

                    </div>


                </div>

                <div className="container mx-auto px-10 ">
                    <hr />
                </div>
                <div className=" py-6 flex mx-auto container">


                    <div className="px-10">
                        <img className="w-24" src="./imageOptionMovie/zion.png" alt="" />
                    </div>
                    <div className="px-20 font-medium text-xs text-white">
                        <p>
                            TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</p>
                        <p>Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.</p>
                        <p>Giấy chứng nhận đăng ký kinh doanh số: 0101659783,</p>
                        <p>đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp.</p>
                        <p>Số Điện Thoại (Hotline): 1900 545 436</p>
                    </div>
                    <div>
                        <img className="w-32" src="./imageOptionMovie/daThongBao-logo.cb85045e.png" alt="" />
                    </div>

                </div>
            </div>
        )



    }


    return (
        <div>
            {renderOptionPage()}

        </div>
    )
}
