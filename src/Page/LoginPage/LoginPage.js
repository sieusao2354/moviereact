import React from 'react'

import { Form, Input, message } from 'antd';
import LoginAnimate from './LoginAnimate';
import { userService } from '../../Service/UserService';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { loginActions } from '../../redux/actions/userActions';
import { localStorageService } from '../../Service/localStorageService';

export default function LoginPage() {

    let dispatch = useDispatch();
    let history = useNavigate()


    const onFinish = (values) => {
        console.log('Success:', values);

        userService.postLogin(values)
            .then((res) => {
                message.success("Đăng nhập thành công");


                dispatch(loginActions(res.data.content));
                localStorageService.user.set(res.data.content)

                setTimeout(() => {
                    history("/")
                }, 1000);
            })
            .catch((err) => {
                message.error(err?.response?.data.content);
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <div>
            <div style={{ background: "url(./image/backApp.png)" }} className='  blueRed h-screen w-screen py-40 mx-auto' >

                < div style={{ width: 1000 }} className='container bg-white mx-auto  rounded-3xl flex' >

                    <div className=' w-1/3 h-96'>
                        <LoginAnimate />


                    </div>

                    <div className='w-1/2 py-20' >
                        <Form

                            name="basic"
                            layout='vertical'


                            labelCol={{
                                span: 8,
                            }}
                            wrapperCol={{
                                span: 24
                                ,
                            }}
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete="off"
                        >
                            <Form.Item

                                label={<p className='font-medium text-black'>Tài khoản</p>}
                                name="taiKhoan"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập tài khoản',
                                    },
                                ]}
                            >
                                <Input className='border' />
                            </Form.Item>

                            <Form.Item
                                label={<p className='font-medium text-black' >Mật khẩu</p>}
                                name="matKhau"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mật khẩu',
                                    },
                                ]}
                            >
                                <Input.Password />
                            </Form.Item>


                            <div className='flex justify-center space-x-5'>
                                <button className='rounded px-40 py-2 text-white bg-red-500 font-medium text-lg hover:bg-red-600    '> Đăng nhập</button>
                            </div><br />
                            <div className='px-28'>



                                <a className='px-14 hover:text-blue-500 font-medium' onClick={() => { window.location.href = "/register" }}>Bạn đã có tài khoản chưa? <br /> <p className='px-16'>Đăng ký ở đây nhé.</p>  </a>
                            </div>


                        </Form>
                    </div>


                </div >
            </div >
        </div >
    )
}
