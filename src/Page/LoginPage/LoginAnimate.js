import React from 'react'
import Lottie from 'lottie-react'

import bgAnimate from "../../assets/LoginAnimate.json"
export default function LoginAnimate() {
    return (
        <div className='transform translate-y-50'>
            <Lottie animationData={bgAnimate} />
        </div>
    )
}
