import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieService } from '../../Service/MovieService'
import { Rate } from 'antd';
import { Progress } from 'antd';


export default function DetailPage() {

    const [Like, setlIKE] = useState({ like: 9999 })
    const handleLike = () => {
        setlIKE({
            like: Like.like + 1
        })
    }
    let { id } = useParams()
    let [movie, setDataMovie] = useState({})

    useEffect(() => {
        movieService.getMovieDetail(id)
            .then((res) => {

                setDataMovie(res.data.content)
            })
            .catch((err) => {
            });

    }, [])
    let renderDetailPage = () => {
        return <div className=' py-20 mx-auto '>
            <div className='flex  p-10 bg-black'>
                <img className='rounded border-spacing-4 ' style={{ height: 500 }} src={movie.hinhAnh} alt="" />

                <div className='px-10 '>
                    <div className='text-white font-medium text-3xl py-2'>Tên phim :  {movie.tenPhim}</div>

                    <p className='text-white font-medium text-xl'> Nội dung: {movie.moTa}</p><br />
                    <button className='text-yellow-300 bg-red-500 px-3 py-3 rounded font-medium text-2xl mx-2'>Đặt Vé </button>
                    <a target="_blank" href={movie.trailer
                    }  >
                        <button className='text-white bg-gray-400 py-3 px-3 rounded font-medium text-2xl'>Trailer</button> <br />
                    </a>
                    <div className='py-20'>
                        <p className='text-white text-lg'>Lượt đánh giá : {Like.like} </p>
                        <button onClick={handleLike} >
                            <Rate allowHalf defaultValue={0} />

                        </button>
                    </div>
                    <div>
                        <Progress type="circle" percent={movie.danhGia * 17} format={(number) => {
                            return <span className='text-white font-medium'> {number / 10} điểm</span>
                        }} />
                    </div>



                </div>


            </div>
        </div>

    }







    return (
        //
        <div>

            {renderDetailPage()}




        </div>


    )
}
