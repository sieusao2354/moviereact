import React from 'react'
import { PacmanLoader } from "react-spinners"
import { useSelector } from 'react-redux'

export default function SpinnerComponent() {

    let { isLoading } = useSelector((state) => {
        return state.spinnerReducer
    })
    return isLoading ? (
        <div className='h-screen w-screen fixed top-0 left-0 overflow-hidden flex justify-center items-center z-50' style={{ backgroundColor: "#6FEDD6" }} >
            <PacmanLoader size={75} />
        </div>
    ) : ("")
}
