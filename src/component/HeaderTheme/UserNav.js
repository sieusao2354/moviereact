import React from 'react'
import { useDispatch, useSelector, } from "react-redux";
import { loginActions } from '../../redux/actions/userActions';
import { localStorageService } from '../../Service/localStorageService';
import { UserOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';

export default function UserNav() {
    let dispatch = useDispatch();
    let userInfor = useSelector((state) => {
        return state.UserReducers.userInfor

    });

    const renderContent = () => {

        if (userInfor) {
            return (
                <div className=' flex space-x-10 items-center'>
                    <p> <div className='text-medium text-white text-lg'> Xin chào</div> <div className='text-medium text-xl text-red-500'>{userInfor.hoTen}</div> </p>
                    <button onClick={handleLogOut} className=' bg-yellow-500  px-4 py-2 rounded-lg font-medium text-lg hover:bg-gray-200 hover:text-black  text-black '> Đăng xuất</button>
                </div>
            );
        } else {

            return (

                <div className='flex items-center '>
                    <button className='  px-4 py-2 rounded-lg font-medium text-lg  hover:bg-red-500 text-white bg-red-600' onClick={() => {
                        window.location.href = "/login"
                    }} >
                        Đăng nhập</button>
                    <button onClick={() => {
                        window.location.href = "/register"
                    }} className=' px-5  mx-5 py-2 rounded-lg text-black bg-yellow-600 hover:bg-yellow-500   font-medium text-lg  text-white'>  Đăng ký</button>
                </div>
            );
        };
    };


    let handleLogOut = () => {
        localStorageService.user.remove();
        dispatch(loginActions(null))
    };
    return (
        <div className=''>{renderContent()}</div>
    );
};
