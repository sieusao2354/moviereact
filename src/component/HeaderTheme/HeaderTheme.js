import React from 'react'
import LogoAnimate from '../../Page/LogoAnimate/LogoAnimate'
import UserNav from './UserNav'

export default function HeaderTheme() {
    return (
        <div style={{ position: 'fixed' }} className=' z-10 w-full h-30 px-10 flex items-center justify-between shadow-lg bg-black '>
            <div style={{ width: 80 }} className=' mt-2 flex '>
                <LogoAnimate />
            </div>
            <div className='' >
                <div className='  text-white news font-medium text-sm py-10 '>
                    <a className=' hover:scale-110 text-blue-400 px-16 ' onClick={() => { window.location.href = "/" }} >Trang chủ</a>
                    <a className=' hover:scale-110 '>Lịch chiếu</a>
                    <a className=' hover:scale-110 px-16 ' href="">Cụm rạp</a>
                    <a className=' hover:scale-110 ' href="">Ứng dụng</a>

                </div>
            </div>
            <UserNav />
        </div>
    )
}
