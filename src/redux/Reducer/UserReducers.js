import { localStorageService } from "../../Service/localStorageService";

export const LOGIN = "LOGIN";
export const REGISTER = "REGISTER";


let initialState = {
    userInfor: localStorageService.user.get(),
    registerData: null,



}

export let UserReducers = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN: {
            state.userInfor = action.payload;
            return { ...state }
        }
        case REGISTER: {
            state.registerData = action.payload;
            return { ...state }
        }

        default:
            return state
    }
}


