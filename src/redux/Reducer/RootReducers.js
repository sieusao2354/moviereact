import { combineReducers } from "redux";
import { MovieReducers } from "./MovieReducers";
import { UserReducers } from "./UserReducers";
import { spinnerReducer } from "./spinnerReducer";

export let RootReducers = combineReducers({

    MovieReducers, UserReducers, spinnerReducer

})