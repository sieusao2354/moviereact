import { BAT_LOADING, TAT_LOADING } from "../constants/spinnerConstant"

export const BatLoadingAction = () => {
    return {
        type: BAT_LOADING
    }
}
export const TatLoadingAction = () => {

    return {
        type: TAT_LOADING
    }
}