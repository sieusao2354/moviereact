import { movieService } from "../../Service/MovieService"
import { SET_MOVIE_LIST } from "../constants/movieConstant";

export const getMovieActionService = () => {

    return (dispatch) => {

        movieService
            .getMovieList()
            .then((res) => {

                dispatch({
                    type: SET_MOVIE_LIST,
                    payload: res.data.content,
                })
            })
            .catch((err) => {
                console.log('err: ', err);
            });
    }
}


