import { https } from "./ConfigURL"

export let movieService = {
    getMovieList: () => {
        return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP10");
    },
    getTabMovie: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05")
    },
    getMovieDetail: (id) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
    },
}


