import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import LoginPage from './Page/LoginPage/LoginPage';

import HomePage from './Page/HomePage/HomePage';
import Layout from './HOC/Layout';
import RegisterPage from './Page/RegisterPage/RegisterPage';
import DetailPage from './Page/DetailPage/DetailPage';
function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout Component={HomePage} />} />;
          <Route path="/detail/:id" element={<Layout Component={DetailPage} />} />
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route path="/register" element={<Layout Component={RegisterPage} />} />;
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
